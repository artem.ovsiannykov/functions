const getSum = (str1, str2) => {
  if (typeof str1 !== "string" || typeof str2 !== "string") return false;
  if (isNaN(+str1) || isNaN(+str2)) return false;
  const str1Length = str1.length;
  const str2Length = str2.length;
  const maxLength = Math.max(str1Length, str2Length);

  let carry = 0,
    sum = "";

  for (let i = 1; i <= str2Length; i++) {
    let a = +str1.charAt(str1Length - i);
    let b = +str2.charAt(str2Length - i);

    let t = carry + a + b;
    carry = (t / 10) | 0;
    t %= 10;

    sum = i === maxLength && carry ? carry * 10 + t + sum : t + sum;
  }

  return sum;
};

const tickets = (people) => {
  let bill25 = 0;
  let bill50 = 0;
  for (let person of people) {
    if (person == 25) {
      bill25 += 1;
    }
    if (person == 50) {
      bill25 -= 1;
      bill50 += 1;
    }
    if (person == 100) {
      if (bill50 == 0 && bill25 >= 3) {
        bill25 -= 3;
      } else {
        bill25 -= 1;
        bill50 -= 1;
      }
    }
    if (bill25 < 0 || bill50 < 0) {
      return "NO";
    }
  }
  return "YES";
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0;
  let comments = 0;
  for (let post of listOfPosts) {
    if (post["author"] === authorName) {
      posts++;
    }
    if (post["comments"]) {
      for (let comment of post["comments"]) {
        if (comment["author"] === authorName) {
          comments++;
        }
      }
    }
  }
  return `Post:${posts},comments:${comments}`;
};

module.exports = { getSum, getQuantityPostsByAuthor, tickets };
